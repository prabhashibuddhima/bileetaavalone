import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';

export interface Employee {
  empID: string;
  fname: string;
  lname: string;
  gender: string;
  dob: string;
  email: string;
  phone: string;
  address: string;
  status: string;

}


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  avaloneEmployees: any = [];
  isAdded: any = "Not Added";
  isDeleted: any = "Not Deleted";
  isUpdated: any = "Not Updated";

  constructor(private storage: Storage, private plt: Platform, private router: Router) {
    this.plt.ready().then(() => {
      this.getEmployees();
    });
  }

  async getEmployees() {
    await this.storage.get('avalone_employees')
      .then(
        data => this.setEmployee(data),
        error => console.error(error)
      );
  }

  async setEmployeeDataInStorage() {
    await this.storage.set("avalone_employees", this.avaloneEmployees);
  }

  setEmployee(empdata) {
    console.log(empdata);

    if (empdata == null) {
      this.avaloneEmployees = [];
    } else {
      this.avaloneEmployees = empdata;
    }
  }

  getAllEmployees() {
    return this.avaloneEmployees;
  }

  async createAnEmployee(emplydata) {

    if (this.avaloneEmployees.length > 0) {
      await this.avaloneEmployees.forEach(element => {
        console.log(element.empID);
        console.log(emplydata.empID);
        if (element.empID == emplydata.empID) {
          this.isAdded = "Not Added";
        } else {
          this.avaloneEmployees.push(emplydata);
          this.setEmployeeDataInStorage();
          this.isAdded = "Added";
        }
      });
    } else {
      this.avaloneEmployees.push(emplydata);
      this.setEmployeeDataInStorage();
      this.isAdded = "Added";
    }



    return this.isAdded;
  }

  async editAnEmployee(emplydata) {

    this.avaloneEmployees.forEach((element,index) => {
      if(element.empID == emplydata.empID){
        this.avaloneEmployees[index] = emplydata;
        this.setEmployeeDataInStorage();
        this.isUpdated = "Updated";
      }else{
        this.isUpdated="Not Updated";
      }
    });


    return this.isUpdated;
  }


  async deleteEmployee(empdata){
    this.avaloneEmployees.forEach((element,index) => {
      if(element.empID == empdata.empID){
        this.avaloneEmployees.splice(index,1);
        this.setEmployeeDataInStorage();
        this.isDeleted = "Deleted";
      }else{
        this.isDeleted = "Not Deleted";
      }
    });
    return this.isDeleted;
  }
}

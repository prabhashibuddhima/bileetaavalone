import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
 
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'main-ui',
    loadChildren: () => import('./pages/main-ui/main-ui.module').then( m => m.MainUIPageModule)
  },
  {
    path: 'create-employee',
    loadChildren: () => import('./pages/create-employee/create-employee.module').then( m => m.CreateEmployeePageModule)
  },
  {
    path: 'view-employee',
    loadChildren: () => import('./pages/view-employee/view-employee.module').then( m => m.ViewEmployeePageModule)
  },
  {
    path: 'edit-employee',
    loadChildren: () => import('./pages/edit-employee/edit-employee.module').then( m => m.EditEmployeePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

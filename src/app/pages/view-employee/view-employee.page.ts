import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.page.html',
  styleUrls: ['./view-employee.page.scss'],
})
export class ViewEmployeePage implements OnInit {
  empID: string;
  fname: string;
  lname: string;
  gender: string;
  dob: string;
  email: string;
  phone: string;
  address: string;
  status: string;

  singleEmployee: any;
  homedata: any;

  constructor(private alertController: AlertController, private empService: EmployeeService, private activatedRoute: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService, private storage: Storage,) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.homedata = {
        'heading': 'Normal text',

      };
    }, 3000);
    this.activatedRoute.queryParams.subscribe(params => {
      this.singleEmployee = params;
      this.empID = params.empID;
      this.fname = params.fname;
      this.lname = params.lname;
      this.gender = params.gender;
      this.dob = params.dob;
      this.email = params.email;
      this.phone = params.phone;
      this.address = params.address;
      this.status = params.status;
    });

  }

  goBack() {
    this.router.navigate(['main-ui']);
  }

  deleteEmployee() {
    this.deleteConfirmAlert();

  }

  editEmployee() {
    this.router.navigate(['edit-employee'], { queryParams: this.singleEmployee });
  }

  async deleteConfirmAlert() {
    const alert = await this.alertController.create({

      message: 'Are you sure you want to delete this employee? ',
      buttons: [
        {
          text: 'Cancel',

        },
        {
          text: 'Ok',
          handler: () => {
            this.empService.deleteEmployee(this.singleEmployee).then((res) => {
              if (res == "Deleted") {
                this.goBack();
                this.successAlert();
              } else {
                this.errAlert();
              }
            });
          }
        }]
    });

    await alert.present();
  }


  async successAlert() {
    const alert = await this.alertController.create({

      message: 'Employee successfully deleted!',
       buttons: [{
        text: 'Ok',
        // handler: () => {
          
        // }
      }]
    });

    await alert.present();
  }

  async errAlert() {
    const alert = await this.alertController.create({

      message: 'Something went wrong! Please try again later!',
       buttons: ['Ok']
    });

    await alert.present();
  }


}

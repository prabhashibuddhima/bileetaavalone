import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { AlertController } from '@ionic/angular';

import { FormGroup } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.page.html',
  styleUrls: ['./edit-employee.page.scss'],
})
export class EditEmployeePage implements OnInit {
  editEmpForm: FormGroup;
  empID: string;
  fname: string;
  lname: string;
  gender: string;
  dob: string;
  email: string;
  phone: string;
  address: string;
  status: string;
  homedata: any;

  constructor(private formBuilder: FormBuilder, private alertController: AlertController, private empService: EmployeeService, private activatedRoute: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService, private storage: Storage,) { 
    this.editEmpForm = formBuilder.group({
      empID: ['', Validators.required],
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      gender: ['', Validators.required],
      dob: ['', Validators.required],
      email: ['', Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')],
      phone: ['', Validators.pattern('[0-9 -()+]+$')],
      address: ['', Validators.required],
      status: ['', Validators.required]

    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.homedata = {
        'heading': 'Normal text',

      };
    }, 3000);
    this.activatedRoute.queryParams.subscribe(params => {
      
      this.empID = params.empID;
      this.fname = params.fname;
      this.lname = params.lname;
      this.gender = params.gender;
      this.dob = params.dob;
      this.email = params.email;
      this.phone = params.phone;
      this.address = params.address;
      this.status = params.status;
    });

  }

  goBack(){
    this.router.navigate(['main-ui']);  
  }

  updateEmployee(){
    let empdata = {
      empID: this.empID,
      fname: this.fname,
      lname: this.lname,
      gender: this.gender,
      dob: this.dob,
      email: this.email,
      phone: this.phone,
      address: this.address,
      status: this.status
    }

    this.empService.editAnEmployee(empdata).then((res) => {
      console.log(res);
      if (res == "Updated"){
        this.successAlert();
      }else{
        this.existAlert();
      }
    });

    

    
  }


  async successAlert() {
    const alert = await this.alertController.create({

      message: 'Employee successfully updated!',
       buttons: [{
        text: 'Ok',
        handler: () => {
          this.goBack();
        }
      }]
    });

    await alert.present();
  }

  async existAlert() {
    const alert = await this.alertController.create({

      message: 'Employee ID already exist!',
       buttons: ['Ok']
    });

    await alert.present();
  }

}

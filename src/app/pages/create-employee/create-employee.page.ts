import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/services/employee.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.page.html',
  styleUrls: ['./create-employee.page.scss'],
})
export class CreateEmployeePage implements OnInit {
  createEmpForm: FormGroup;
  empID: string;
  fname: string;
  lname: string;
  gender: string;
  dob: string;
  email: string;
  phone: string;
  address: string;
  status: string;
  homedata: any;

  constructor(private alertController: AlertController, private empService: EmployeeService, private formBuilder: FormBuilder, private router: Router, private authenticationService: AuthenticationService, private storage: Storage,) { 
    this.createEmpForm = formBuilder.group({
      empID: ['', Validators.required],
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      gender: ['', Validators.required],
      dob: ['', Validators.required],
      email: ['', Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')],
      phone: ['', Validators.pattern('[0-9 -()+]+$')],
      address: ['', Validators.required],
      status: ['', Validators.required]

    });
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    setTimeout(() => {
      this.homedata = {
        'heading': 'Normal text',

      };
    }, 3000);
  }

  goBack(){
    this.router.navigate(['main-ui']);  
  }

  createEmployee(){
    let empdata = {
      empID: this.empID,
      fname: this.fname,
      lname: this.lname,
      gender: this.gender,
      dob: this.dob,
      email: this.email,
      phone: this.phone,
      address: this.address,
      status: this.status
    }

    this.empService.createAnEmployee(empdata).then((res) => {
      console.log(res);
      if (res == "Added"){
        this.successAlert();
      }else{
        this.existAlert();
      }
    });

    this.createEmpForm.reset();

    
  }


  async successAlert() {
    const alert = await this.alertController.create({

      message: 'Employee successfully created!',
       buttons: [{
        text: 'Ok',
        handler: () => {
          this.goBack();
        }
      }]
    });

    await alert.present();
  }

  async existAlert() {
    const alert = await this.alertController.create({

      message: 'Employee ID already exist!',
       buttons: ['OK']
    });

    await alert.present();
  }
}

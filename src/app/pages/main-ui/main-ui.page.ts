import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Employee, EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-main-ui',
  templateUrl: './main-ui.page.html',
  styleUrls: ['./main-ui.page.scss'],
})
export class MainUIPage implements OnInit {

  employeeAvalone: Employee[] = [];
  hasEmployees: boolean = false;
  homedata: any;

  constructor(private empService: EmployeeService, private router: Router, private authenticationService: AuthenticationService, private storage: Storage, ) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    setTimeout(() => {
      this.homedata = {
        'heading': 'Normal text',

      };
    }, 3000);
    this.employeeAvalone = this.empService.getAllEmployees();
    console.log(this.employeeAvalone);
    if(this.employeeAvalone.length == 0){
      this.hasEmployees = false;
    }else{
      this.hasEmployees = true;
    }
  }


  logout(){
    this.authenticationService.logout();
  }

  goToAddEmployee(){
    this.router.navigate(['create-employee']);  
  }

  viewDetails(empdata){
    this.router.navigate(['view-employee'], { queryParams: empdata });  
  }

}

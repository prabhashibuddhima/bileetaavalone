import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainUIPage } from './main-ui.page';

const routes: Routes = [
  {
    path: '',
    component: MainUIPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainUIPageRoutingModule {}

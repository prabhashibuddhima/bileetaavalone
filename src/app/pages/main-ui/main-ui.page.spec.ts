import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MainUIPage } from './main-ui.page';

describe('MainUIPage', () => {
  let component: MainUIPage;
  let fixture: ComponentFixture<MainUIPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainUIPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MainUIPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

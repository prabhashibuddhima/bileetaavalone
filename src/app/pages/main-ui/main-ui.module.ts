import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MainUIPageRoutingModule } from './main-ui-routing.module';

import { MainUIPage } from './main-ui.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainUIPageRoutingModule
  ],
  declarations: [MainUIPage]
})
export class MainUIPageModule {}

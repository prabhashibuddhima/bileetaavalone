import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  username: any;
  password: any;


  constructor(private router: Router, private formBuilder: FormBuilder, private alertController: AlertController,private authService: AuthenticationService) { 
    this.loginForm = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  loginData(){
    if (this.username == 'Admin' && this.password == '12345'){
      this.authService.loginAuthenticate();
      this.successAlert();
      this.router.navigate(['main-ui']);  
    }else{
      this.errAlert();
    }
  }

  async successAlert() {
    const alert = await this.alertController.create({

      message: 'Successfully Logged!',
       buttons: ['OK']
    });

    await alert.present();
  }

  async errAlert() {
    const alert = await this.alertController.create({

      message: 'Username or Password incorrect! Please try again!',
       buttons: ['Ok']
    });

    await alert.present();
  }

}

(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- creted by prabhashibuddhima -->\n<ion-content class=\"login-content-back\" fullscreen>\n  <ion-grid>\n    <ion-row class=\"logo-row\">\n      <ion-col>\n        <div>\n          <img src=\"../../../assets/no_bg_logo.png\" align=\"center\" class=\"loginpage-logo\" />\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row style=\"margin-top: 40vw; margin-left: 12vw;\">\n      <ion-col>\n        <div class=\"login-topic\">\n          Login\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <form [formGroup]=\"loginForm\" (ngSubmit)=\"loginData()\">\n      <ion-row class=\"input-rows\">\n        <ion-item lines=\"none\" class=\"input-itms\">\n          <ion-input id=\"username\" class=\"login-inputs\" type=\"text\" placeholder=\"Username\" formControlName=\"username\"\n            [(ngModel)]=\"username\" name=\"username\" required></ion-input>\n        </ion-item>\n\n      </ion-row>\n      <ion-row class=\"input-rows\">\n        <ion-item\n          *ngIf=\"loginForm != null && loginForm.get('username').valid && loginForm.get('username').touched && !loginForm.get('username').valid\"\n          lines=\"none\" class=\"warningMsg\">\n          <ion-label color=\"danger\">\n            Please Enter Your Username\n          </ion-label>\n        </ion-item>\n      </ion-row>\n\n      <ion-row class=\"password-row\">\n        <ion-item lines=\"none\" class=\"input-itms\">\n          <ion-input id=\"password\" class=\"login-inputs\" type=\"password\" placeholder=\"Password\"\n            formControlName=\"password\" [(ngModel)]=\"password\" name=\"password\" required></ion-input>\n        </ion-item>\n      </ion-row>\n\n      <ion-row class=\"input-rows\">\n        <ion-item\n          *ngIf=\"loginForm != null && loginForm.get('password').valid && loginForm.get('password').touched && !loginForm.get('password').valid\"\n          lines=\"none\" class=\"warningMsg\">\n          <ion-label color=\"danger\">\n            Please Enter Your Password\n          </ion-label>\n        </ion-item>\n      </ion-row>\n\n      <ion-row class=\"input-rows\">\n        <ion-col>\n          <ion-button class=\"login-btn\" type=\"submit\"\n            [disabled]=\"username == null || password == null || !loginForm.get('username').valid || username == '' || password == '' \">\n            Login\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </form>\n  </ion-grid>\n\n\n\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/login/login-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/login/login-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function srcAppPagesLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/pages/login/login.page.ts");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/login/login.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/pages/login/login.module.ts ***!
      \*********************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function srcAppPagesLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "./src/app/pages/login/login-routing.module.ts");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/pages/login/login.page.ts");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "./src/app/pages/login/login.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/pages/login/login.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".login-content-back {\n  --background: #fff url('loginbg2.png') no-repeat center center / cover;\n}\n\n.loginpage-logo {\n  margin: auto;\n  display: block;\n  height: auto;\n  width: 32%;\n}\n\n.logo-row {\n  margin-top: 20vw;\n}\n\n@media only screen and (max-width: 280px) and (min-width: 0px) {\n  .logo-row {\n    margin-top: 36vw;\n  }\n}\n\n@media only screen and (max-width: 375px) and (min-width: 361px) and (max-height: 812px) and (min-height: 668px) {\n  .logo-row {\n    margin-top: 32vw;\n  }\n}\n\n@media only screen and (max-width: 412px) and (min-width: 376px) {\n  .logo-row {\n    margin-top: 14vw;\n  }\n}\n\n@media only screen and (device-width: 414px) and (device-height: 896px) {\n  .logo-row {\n    margin-top: 32vw;\n  }\n}\n\n@media only screen and (max-width: 767px) and (min-width: 539px) {\n  .logo-row {\n    margin-top: 2vw;\n  }\n}\n\n@media only screen and (max-width: 779px) and (min-width: 768px) {\n  .logo-row {\n    margin-top: 0vw;\n  }\n}\n\n@media only screen and (max-width: 1039px) and (min-width: 829px) {\n  .logo-row {\n    margin-top: 0vw;\n  }\n}\n\n@media only screen and (max-width: 1299px) and (min-width: 1040px) {\n  .logo-row {\n    margin-top: 0vw;\n  }\n}\n\n@media only screen and (min-width: 1300px) {\n  .logo-row {\n    margin-top: 0vw;\n  }\n}\n\n.login-topic {\n  font-size: 8vw;\n  color: white;\n  font-weight: 500;\n  font-family: \"Lucida Sans\", \"Lucida Sans Regular\", \"Lucida Grande\", \"Lucida Sans Unicode\", Geneva, Verdana, sans-serif;\n}\n\n.input-rows {\n  margin-left: 12vw;\n  margin-top: 6vw;\n}\n\n.password-row {\n  margin-left: 12vw;\n  margin-top: 3vw;\n}\n\n.input-itms {\n  width: 80%;\n  border-radius: 2vw;\n  --background: white;\n}\n\n.login-inputs {\n  --color: black;\n  --placeholder-color: #978F8F;\n  font-family: \"Lucida Sans\", \"Lucida Sans Regular\", \"Lucida Grande\", \"Lucida Sans Unicode\", Geneva, Verdana, sans-serif;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBSUksc0VBQUE7QUFGSjs7QUFLQTtFQUNJLFlBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUFGSjs7QUFLQTtFQUNJLGdCQUFBO0FBRko7O0FBSUk7RUFISjtJQUlRLGdCQUFBO0VBRE47QUFDRjs7QUFLSTtFQVRKO0lBVVEsZ0JBQUE7RUFGTjtBQUNGOztBQU9JO0VBaEJKO0lBaUJRLGdCQUFBO0VBSk47QUFDRjs7QUFRSTtFQXRCSjtJQXlCUSxnQkFBQTtFQVBOO0FBQ0Y7O0FBVUk7RUE3Qko7SUE4QlEsZUFBQTtFQVBOO0FBQ0Y7O0FBV0k7RUFuQ0o7SUFvQ1EsZUFBQTtFQVJOO0FBQ0Y7O0FBYUk7RUExQ0o7SUEyQ1EsZUFBQTtFQVZOO0FBQ0Y7O0FBWUk7RUE5Q0o7SUErQ1EsZUFBQTtFQVROO0FBQ0Y7O0FBV0k7RUFsREo7SUFtRFEsZUFBQTtFQVJOO0FBQ0Y7O0FBV0E7RUFDSSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esc0hBQUE7QUFSSjs7QUFXQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQVJKOztBQVdBO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0FBUko7O0FBV0E7RUFDSSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQVJKOztBQVdBO0VBQ0ksY0FBQTtFQUNBLDRCQUFBO0VBQ0Esc0hBQUE7QUFSSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dpbi1jb250ZW50LWJhY2sge1xuICAgIC8vIC0tYmFja2dyb3VuZDogbm9uZTtcbiAgICAvLyBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2xvZ2luYmcucG5nKSBuby1yZXBlYXQgMCAwO1xuICAgIC8vIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmIHVybCgnLi4vLi4vLi4vYXNzZXRzL2xvZ2luYmcyLnBuZycpIG5vLXJlcGVhdCBjZW50ZXIgY2VudGVyIC8gY292ZXI7XG59XG5cbi5sb2dpbnBhZ2UtbG9nb3tcbiAgICBtYXJnaW46IGF1dG87XG4gICAgZGlzcGxheTogYmxvY2s7IFxuICAgIGhlaWdodDogYXV0bzsgXG4gICAgd2lkdGg6IDMyJTtcbn1cblxuLmxvZ28tcm93e1xuICAgIG1hcmdpbi10b3A6IDIwdnc7XG4gICAgLy9nYWxheHkgZm9sZFxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMjgwcHgpIGFuZCAobWluLXdpZHRoOiAwcHgpe1xuICAgICAgICBtYXJnaW4tdG9wOiAzNnZ3IDtcbiAgICB9XG5cbiAgICBcbiAgICAvL2lwaG9uZSB4XG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzNzVweCkgYW5kIChtaW4td2lkdGg6IDM2MXB4KSBhbmQgKG1heC1oZWlnaHQ6IDgxMnB4KSBhbmQgKG1pbi1oZWlnaHQ6IDY2OHB4KXtcbiAgICAgICAgbWFyZ2luLXRvcDogMzJ2dztcbiAgICB9XG5cblxuXG4gICAgLy9uZXh1cyA1eFxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDEycHgpIGFuZCAobWluLXdpZHRoOiAzNzZweCl7XG4gICAgICAgIG1hcmdpbi10b3A6IDE0dnc7XG4gICAgfVxuXG5cbiAgICAvL2lwaG9uZSBYUlxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBcbiAgICBhbmQgKGRldmljZS13aWR0aCA6IDQxNHB4KSBcbiAgICBhbmQgKGRldmljZS1oZWlnaHQgOiA4OTZweCkgIHtcbiAgICAgICAgbWFyZ2luLXRvcDogMzJ2dztcbiAgICB9XG5cbiAgICAvL3N1cmZkdW9cbiAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2N3B4KSBhbmQgKG1pbi13aWR0aDogNTM5cHgpe1xuICAgICAgICBtYXJnaW4tdG9wOiAydnc7XG4gICAgfVxuXG5cbiAgICAvL2lwYWQgbWluaSwgaXBhZFxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzc5cHgpIGFuZCAobWluLXdpZHRoOiA3NjhweCl7XG4gICAgICAgIG1hcmdpbi10b3A6IDB2dztcblxuICAgIH1cblxuXG4gICAgLy9pcGFkIHByb1xuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTAzOXB4KSBhbmQgKG1pbi13aWR0aDogODI5cHgpe1xuICAgICAgICBtYXJnaW4tdG9wOiAwdnc7XG4gICAgfVxuXG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMjk5cHgpIGFuZCAobWluLXdpZHRoOiAxMDQwcHgpe1xuICAgICAgICBtYXJnaW4tdG9wOiAwdnc7XG4gICAgfVxuXG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMzAwcHgpIHtcbiAgICAgICAgbWFyZ2luLXRvcDogMHZ3O1xuICAgIH1cbn1cblxuLmxvZ2luLXRvcGlje1xuICAgIGZvbnQtc2l6ZTogOHZ3O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGZvbnQtZmFtaWx5OiAnTHVjaWRhIFNhbnMnLCAnTHVjaWRhIFNhbnMgUmVndWxhcicsICdMdWNpZGEgR3JhbmRlJywgJ0x1Y2lkYSBTYW5zIFVuaWNvZGUnLCBHZW5ldmEsIFZlcmRhbmEsIHNhbnMtc2VyaWY7XG59XG5cbi5pbnB1dC1yb3dze1xuICAgIG1hcmdpbi1sZWZ0OiAxMnZ3O1xuICAgIG1hcmdpbi10b3A6IDZ2dztcbn1cblxuLnBhc3N3b3JkLXJvd3tcbiAgICBtYXJnaW4tbGVmdDogMTJ2dztcbiAgICBtYXJnaW4tdG9wOiAzdnc7XG59XG5cbi5pbnB1dC1pdG1ze1xuICAgIHdpZHRoOiA4MCU7XG4gICAgYm9yZGVyLXJhZGl1czogMnZ3O1xuICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbi5sb2dpbi1pbnB1dHN7XG4gICAgLS1jb2xvcjogYmxhY2s7XG4gICAgLS1wbGFjZWhvbGRlci1jb2xvcjogIzk3OEY4RjtcbiAgICBmb250LWZhbWlseTogJ0x1Y2lkYSBTYW5zJywgJ0x1Y2lkYSBTYW5zIFJlZ3VsYXInLCAnTHVjaWRhIEdyYW5kZScsICdMdWNpZGEgU2FucyBVbmljb2RlJywgR2VuZXZhLCBWZXJkYW5hLCBzYW5zLXNlcmlmO1xufVxuXG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/login/login.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/pages/login/login.page.ts ***!
      \*******************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppPagesLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/authentication.service */
      "./src/app/services/authentication.service.ts");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(router, formBuilder, alertController, authService) {
          _classCallCheck(this, LoginPage);

          this.router = router;
          this.formBuilder = formBuilder;
          this.alertController = alertController;
          this.authService = authService;
          this.loginForm = formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
          });
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "loginData",
          value: function loginData() {
            if (this.username == 'Admin' && this.password == '12345') {
              this.authService.loginAuthenticate();
              this.successAlert();
              this.router.navigate(['main-ui']);
            } else {
              this.errAlert();
            }
          }
        }, {
          key: "successAlert",
          value: function successAlert() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var alert;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.alertController.create({
                        message: 'Successfully Logged!',
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context.sent;
                      _context.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "errAlert",
          value: function errAlert() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertController.create({
                        message: 'Username or Password incorrect! Please try again!',
                        buttons: ['Ok']
                      });

                    case 2:
                      alert = _context2.sent;
                      _context2.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/pages/login/login.page.scss"))["default"]]
      })], LoginPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-login-login-module-es5.js.map
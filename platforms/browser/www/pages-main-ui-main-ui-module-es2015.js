(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-main-ui-main-ui-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main-ui/main-ui.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main-ui/main-ui.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- created by prabhashibuddhima -->\n<ion-content class=\"mainui-content-back\" fullscreen *ngIf=\"homedata\">\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <div style=\"text-align: end;\">\n          <ion-icon name=\"log-out-outline\" style=\"zoom: 2\" (click)=\"logout()\"></ion-icon>\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"logo-row\">\n      <ion-col>\n        <div>\n          <img src=\"../../../assets/no_bg_logo.png\" align=\"center\" class=\"mainuipage-logo\" />\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row style=\"margin-top: 14vw;\">\n      <ion-col>\n        <ion-item class=\"name-topic-item\">\n          <ion-label class=\"name-topic\">\n            Employees\n          </ion-label>\n        </ion-item>\n      </ion-col>\n\n    </ion-row>\n\n    <div *ngIf=\"hasEmployees == true\">\n      <ion-row *ngFor=\"let emp of employeeAvalone\">\n        <ion-col>\n          <ion-item lines=\"none\" class=\"empname-cls\">\n            {{emp.fname}} {{emp.lname}}\n            <ion-icon name=\"reader-outline\" slot=\"end\" (click)=\"viewDetails(emp)\"></ion-icon>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </div>\n\n\n\n    <ion-row *ngIf=\"hasEmployees == false\" style=\"margin-top: 20vw;margin-left: 8vw;\">\n      <ion-col>\n        <ion-item lines=\"none\">\n          No employees available to show. Add employees by clicking \"+\" at the bottom right corner.\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n\n\n  </ion-grid>\n\n\n\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button (click)=\"goToAddEmployee()\">\n      <ion-icon name=\"add\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n\n</ion-content>\n\n<ion-content *ngIf=\"!homedata\">\n  <ion-grid style=\"margin-top: 6vh\">\n    <ion-row style=\"margin-top: 40vh;text-align: center\">\n      <ion-col>\n        <div>\n          <ion-spinner name=\"circles\" style=\"width: 20vw;color: #2B71B6; transform: scale(1.5); flex-direction: column\">\n          </ion-spinner>\n        </div>\n\n      </ion-col>\n    </ion-row>\n\n  </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/main-ui/main-ui-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/main-ui/main-ui-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: MainUIPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainUIPageRoutingModule", function() { return MainUIPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _main_ui_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main-ui.page */ "./src/app/pages/main-ui/main-ui.page.ts");




const routes = [
    {
        path: '',
        component: _main_ui_page__WEBPACK_IMPORTED_MODULE_3__["MainUIPage"]
    }
];
let MainUIPageRoutingModule = class MainUIPageRoutingModule {
};
MainUIPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MainUIPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/main-ui/main-ui.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/main-ui/main-ui.module.ts ***!
  \*************************************************/
/*! exports provided: MainUIPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainUIPageModule", function() { return MainUIPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _main_ui_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./main-ui-routing.module */ "./src/app/pages/main-ui/main-ui-routing.module.ts");
/* harmony import */ var _main_ui_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./main-ui.page */ "./src/app/pages/main-ui/main-ui.page.ts");







let MainUIPageModule = class MainUIPageModule {
};
MainUIPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _main_ui_routing_module__WEBPACK_IMPORTED_MODULE_5__["MainUIPageRoutingModule"]
        ],
        declarations: [_main_ui_page__WEBPACK_IMPORTED_MODULE_6__["MainUIPage"]]
    })
], MainUIPageModule);



/***/ }),

/***/ "./src/app/pages/main-ui/main-ui.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/main-ui/main-ui.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".empname-cls {\n  --background: #A8A7CA;\n  width: 86vw;\n  margin-top: 3vw;\n  border-radius: 2vw;\n  margin-left: 4vw;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFpbi11aS9tYWluLXVpLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBREoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tYWluLXVpL21haW4tdWkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG5cbi5lbXBuYW1lLWNsc3tcbiAgICAtLWJhY2tncm91bmQ6ICNBOEE3Q0E7XG4gICAgd2lkdGg6IDg2dnc7XG4gICAgbWFyZ2luLXRvcDogM3Z3O1xuICAgIGJvcmRlci1yYWRpdXM6IDJ2dztcbiAgICBtYXJnaW4tbGVmdDogNHZ3O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/main-ui/main-ui.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/main-ui/main-ui.page.ts ***!
  \***********************************************/
/*! exports provided: MainUIPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainUIPage", function() { return MainUIPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_employee_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/employee.service */ "./src/app/services/employee.service.ts");






let MainUIPage = class MainUIPage {
    constructor(empService, router, authenticationService, storage) {
        this.empService = empService;
        this.router = router;
        this.authenticationService = authenticationService;
        this.storage = storage;
        this.employeeAvalone = [];
        this.hasEmployees = false;
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        setTimeout(() => {
            this.homedata = {
                'heading': 'Normal text',
            };
        }, 3000);
        this.employeeAvalone = this.empService.getAllEmployees();
        console.log(this.employeeAvalone);
        if (this.employeeAvalone.length == 0) {
            this.hasEmployees = false;
        }
        else {
            this.hasEmployees = true;
        }
    }
    logout() {
        this.authenticationService.logout();
    }
    goToAddEmployee() {
        this.router.navigate(['create-employee']);
    }
    viewDetails(empdata) {
        this.router.navigate(['view-employee'], { queryParams: empdata });
    }
};
MainUIPage.ctorParameters = () => [
    { type: src_app_services_employee_service__WEBPACK_IMPORTED_MODULE_5__["EmployeeService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] }
];
MainUIPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-main-ui',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./main-ui.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main-ui/main-ui.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./main-ui.page.scss */ "./src/app/pages/main-ui/main-ui.page.scss")).default]
    })
], MainUIPage);



/***/ })

}]);
//# sourceMappingURL=pages-main-ui-main-ui-module-es2015.js.map
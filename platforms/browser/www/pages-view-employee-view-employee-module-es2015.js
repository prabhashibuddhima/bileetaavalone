(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-view-employee-view-employee-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/view-employee/view-employee.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/view-employee/view-employee.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- created by prabhashibuddhima -->\n<ion-content class=\"mainui-content-back\" fullscreen  *ngIf=\"homedata\">\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <div style=\"text-align: start;\">\n\n          <ion-icon name=\"arrow-back-outline\" style=\"zoom: 2\" (click)=\"goBack()\"></ion-icon>\n\n        </div>\n\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"logo-row\">\n      <ion-col>\n        <div>\n          <img src=\"../../../assets/no_bg_logo.png\" align=\"center\" class=\"mainuipage-logo\" />\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row style=\"margin-top: 14vw;\">\n      <ion-col>\n        <ion-item class=\"name-topic-item\">\n          <div>\n            <div class=\"name-topic\" style=\"text-align: start;\">\n              {{fname}} {{lname}}\n            </div>\n            <div style=\"font-size: 3vw;color: #767676;\">\n              Emp Id: {{empID}}\n            </div>\n          </div>\n\n\n        </ion-item>\n      </ion-col>\n\n    </ion-row>\n\n\n   \n\n    <ion-row>\n      <ion-col>\n        <ion-item lines=\"none\" class=\"empinputs\">\n          <ion-label position=\"floating\" class=\"floatinglbl\">First Name</ion-label>\n          <ion-input id=\"fname\" class=\"login-inputs\" type=\"text\" [(ngModel)]=\"fname\" name=\"fname\" disabled></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col>\n        <ion-item lines=\"none\" class=\"empinputs\">\n          <ion-label position=\"floating\" class=\"floatinglbl\">Last Name</ion-label>\n          <ion-input id=\"lname\" class=\"login-inputs\" type=\"text\" [(ngModel)]=\"lname\" name=\"lname\" disabled></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col>\n        <ion-item lines=\"none\" class=\"empinputs\">\n          <ion-label position=\"floating\" class=\"floatinglbl\">Gender</ion-label>\n          <!-- <ion-input ></ion-input> -->\n          <ion-select id=\"gender\" class=\"login-inputs\" type=\"text\" [(ngModel)]=\"gender\" name=\"gender\" disabled>\n            <ion-select-option value=\"Male\">Male</ion-select-option>\n            <ion-select-option value=\"Female\">Female</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col>\n        <ion-item lines=\"none\" class=\"empinputs\">\n          <ion-label position=\"floating\" class=\"floatinglbl\">Date of Birth</ion-label>\n          <ion-datetime id=\"dob\" class=\"login-inputs\" type=\"text\" [(ngModel)]=\"dob\" name=\"dob\" disabled></ion-datetime>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col>\n        <ion-item lines=\"none\" class=\"empinputs\">\n          <ion-label position=\"floating\" class=\"floatinglbl\">Email</ion-label>\n          <ion-input id=\"email\" class=\"login-inputs\" type=\"text\" [(ngModel)]=\"email\" name=\"email\" disabled></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n\n\n    <ion-row>\n      <ion-col>\n        <ion-item lines=\"none\" class=\"empinputs\">\n          <ion-label position=\"floating\" class=\"floatinglbl\">Phone</ion-label>\n          <ion-input id=\"phone\" class=\"login-inputs\" type=\"text\" [(ngModel)]=\"phone\" name=\"phone\" disabled></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row>\n      <ion-col>\n        <ion-item lines=\"none\" class=\"empinputs\">\n          <ion-label position=\"floating\" class=\"floatinglbl\">Address</ion-label>\n          <ion-input id=\"address\" class=\"login-inputs\" type=\"text\" [(ngModel)]=\"address\" name=\"address\" disabled>\n          </ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col>\n        <ion-item lines=\"none\" class=\"empinputs\">\n          <ion-label position=\"floating\" class=\"floatinglbl\">Status</ion-label>\n          <!-- <ion-input ></ion-input> -->\n          <ion-select id=\"status\" class=\"login-inputs\" type=\"text\" [(ngModel)]=\"status\" name=\"status\" disabled>\n            <ion-select-option value=\"Single\">Single</ion-select-option>\n            <ion-select-option value=\"Married\">Married</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row style=\"margin-left: 4vw; margin-top: 3vw; margin-bottom: 8vw;\">\n      <ion-col>\n        <ion-button class=\"edit-btn\" (click)=\"editEmployee()\">\n          <ion-icon name=\"create-outline\" style=\"color: white;\"></ion-icon>\n        </ion-button>\n      </ion-col>\n\n      <ion-col>\n        <ion-button class=\"dlt-btn\" (click)=\"deleteEmployee()\">\n          <ion-icon name=\"trash-outline\" style=\"color: white;\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n\n\n\n\n\n\n\n\n  </ion-grid>\n\n\n\n</ion-content>\n\n\n<ion-content *ngIf=\"!homedata\">\n  <ion-grid style=\"margin-top: 6vh\">\n    <ion-row style=\"margin-top: 40vh;text-align: center\">\n      <ion-col>\n        <div>\n          <ion-spinner name=\"circles\" style=\"width: 20vw;color: #2B71B6; transform: scale(1.5); flex-direction: column\">\n          </ion-spinner>\n        </div>\n\n      </ion-col>\n    </ion-row>\n\n  </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/view-employee/view-employee-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/view-employee/view-employee-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: ViewEmployeePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewEmployeePageRoutingModule", function() { return ViewEmployeePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _view_employee_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./view-employee.page */ "./src/app/pages/view-employee/view-employee.page.ts");




const routes = [
    {
        path: '',
        component: _view_employee_page__WEBPACK_IMPORTED_MODULE_3__["ViewEmployeePage"]
    }
];
let ViewEmployeePageRoutingModule = class ViewEmployeePageRoutingModule {
};
ViewEmployeePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ViewEmployeePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/view-employee/view-employee.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/view-employee/view-employee.module.ts ***!
  \*************************************************************/
/*! exports provided: ViewEmployeePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewEmployeePageModule", function() { return ViewEmployeePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _view_employee_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./view-employee-routing.module */ "./src/app/pages/view-employee/view-employee-routing.module.ts");
/* harmony import */ var _view_employee_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./view-employee.page */ "./src/app/pages/view-employee/view-employee.page.ts");







let ViewEmployeePageModule = class ViewEmployeePageModule {
};
ViewEmployeePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _view_employee_routing_module__WEBPACK_IMPORTED_MODULE_5__["ViewEmployeePageRoutingModule"]
        ],
        declarations: [_view_employee_page__WEBPACK_IMPORTED_MODULE_6__["ViewEmployeePage"]]
    })
], ViewEmployeePageModule);



/***/ }),

/***/ "./src/app/pages/view-employee/view-employee.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/view-employee/view-employee.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".dlt-btn {\n  width: 86%;\n  text-transform: none;\n  --color: white;\n  --background: #c25353;\n  --border-radius: 2vw;\n  height: 12vw;\n}\n\n.edit-btn {\n  width: 86%;\n  text-transform: none;\n  --color: white;\n  --background: #2B71B6;\n  --border-radius: 2vw;\n  height: 12vw;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdmlldy1lbXBsb3llZS92aWV3LWVtcGxveWVlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFVBQUE7RUFDQSxvQkFBQTtFQUNBLGNBQUE7RUFDQSxxQkFBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksVUFBQTtFQUNBLG9CQUFBO0VBQ0EsY0FBQTtFQUNBLHFCQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy92aWV3LWVtcGxveWVlL3ZpZXctZW1wbG95ZWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRsdC1idG57XG4gICAgd2lkdGg6IDg2JTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICAtLWJhY2tncm91bmQ6ICNjMjUzNTM7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAydnc7XG4gICAgaGVpZ2h0OiAxMnZ3O1xufVxuXG4uZWRpdC1idG57XG4gICAgd2lkdGg6IDg2JTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICAtLWJhY2tncm91bmQ6ICMyQjcxQjY7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAydnc7XG4gICAgaGVpZ2h0OiAxMnZ3O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/view-employee/view-employee.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/view-employee/view-employee.page.ts ***!
  \***********************************************************/
/*! exports provided: ViewEmployeePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewEmployeePage", function() { return ViewEmployeePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_services_employee_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");







let ViewEmployeePage = class ViewEmployeePage {
    constructor(alertController, empService, activatedRoute, router, authenticationService, storage) {
        this.alertController = alertController;
        this.empService = empService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.authenticationService = authenticationService;
        this.storage = storage;
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        setTimeout(() => {
            this.homedata = {
                'heading': 'Normal text',
            };
        }, 3000);
        this.activatedRoute.queryParams.subscribe(params => {
            this.singleEmployee = params;
            this.empID = params.empID;
            this.fname = params.fname;
            this.lname = params.lname;
            this.gender = params.gender;
            this.dob = params.dob;
            this.email = params.email;
            this.phone = params.phone;
            this.address = params.address;
            this.status = params.status;
        });
    }
    goBack() {
        this.router.navigate(['main-ui']);
    }
    deleteEmployee() {
        this.deleteConfirmAlert();
    }
    editEmployee() {
        this.router.navigate(['edit-employee'], { queryParams: this.singleEmployee });
    }
    deleteConfirmAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message: 'Are you sure you want to delete this employee? ',
                buttons: [
                    {
                        text: 'Cancel',
                    },
                    {
                        text: 'Ok',
                        handler: () => {
                            this.empService.deleteEmployee(this.singleEmployee).then((res) => {
                                if (res == "Deleted") {
                                    this.goBack();
                                    this.successAlert();
                                }
                                else {
                                    this.errAlert();
                                }
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    successAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message: 'Employee successfully deleted!',
                buttons: [{
                        text: 'Ok',
                    }]
            });
            yield alert.present();
        });
    }
    errAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message: 'Something went wrong! Please try again later!',
                buttons: ['Ok']
            });
            yield alert.present();
        });
    }
};
ViewEmployeePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: src_app_services_employee_service__WEBPACK_IMPORTED_MODULE_5__["EmployeeService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] }
];
ViewEmployeePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-view-employee',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./view-employee.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/view-employee/view-employee.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./view-employee.page.scss */ "./src/app/pages/view-employee/view-employee.page.scss")).default]
    })
], ViewEmployeePage);



/***/ })

}]);
//# sourceMappingURL=pages-view-employee-view-employee-module-es2015.js.map
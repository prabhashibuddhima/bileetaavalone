(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-create-employee-create-employee-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/create-employee/create-employee.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/create-employee/create-employee.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- created by prabhashibuddhima -->\n<ion-content class=\"mainui-content-back\" fullscreen *ngIf=\"homedata\">\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <div style=\"text-align: start;\">\n         \n          <ion-icon name=\"arrow-back-outline\" style=\"zoom: 2\" (click)=\"goBack()\"></ion-icon>\n         \n        </div>\n        \n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"logo-row\">\n      <ion-col>\n        <div>\n          <img src=\"../../../assets/no_bg_logo.png\" align=\"center\" class=\"mainuipage-logo\" />\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row style=\"margin-top: 14vw;\">\n      <ion-col>\n        <ion-item class=\"name-topic-item\">\n          <ion-label class=\"name-topic\">\n            Create an Employee\n          </ion-label>\n        </ion-item>\n      </ion-col>\n\n    </ion-row>\n\n    <form [formGroup]=\"createEmpForm\" (ngSubmit)=\"createEmployee()\">\n      <ion-row>\n        <ion-col>\n          <ion-item lines=\"none\" class=\"empinputs\">\n            <ion-label position=\"floating\" class=\"floatinglbl\">Employee ID</ion-label>\n            <ion-input id=\"empID\" class=\"login-inputs\" type=\"text\" formControlName=\"empID\"\n            [(ngModel)]=\"empID\" name=\"empID\" required></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item lines=\"none\" class=\"empinputs\">\n            <ion-label position=\"floating\" class=\"floatinglbl\">First Name</ion-label>\n            <ion-input id=\"fname\" class=\"login-inputs\" type=\"text\" formControlName=\"fname\"\n            [(ngModel)]=\"fname\" name=\"fname\" required></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item lines=\"none\" class=\"empinputs\">\n            <ion-label position=\"floating\" class=\"floatinglbl\">Last Name</ion-label>\n            <ion-input id=\"lname\" class=\"login-inputs\" type=\"text\" formControlName=\"lname\"\n            [(ngModel)]=\"lname\" name=\"lname\" required></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item lines=\"none\" class=\"empinputs\">\n            <ion-label position=\"floating\" class=\"floatinglbl\">Gender</ion-label>\n            <!-- <ion-input ></ion-input> -->\n            <ion-select id=\"gender\" class=\"login-inputs\" type=\"text\" formControlName=\"gender\"\n            [(ngModel)]=\"gender\" name=\"gender\" required>\n              <ion-select-option value=\"Male\">Male</ion-select-option>\n              <ion-select-option value=\"Female\">Female</ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item lines=\"none\" class=\"empinputs\">\n            <ion-label position=\"floating\" class=\"floatinglbl\">Date of Birth</ion-label>\n            <ion-datetime id=\"dob\" class=\"login-inputs\" type=\"text\" formControlName=\"dob\"\n            [(ngModel)]=\"dob\" name=\"dob\" required></ion-datetime>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item lines=\"none\" class=\"empinputs\">\n            <ion-label position=\"floating\" class=\"floatinglbl\">Email</ion-label>\n            <ion-input id=\"email\" class=\"login-inputs\" type=\"text\" formControlName=\"email\"\n            [(ngModel)]=\"email\" name=\"email\" required></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item\n          *ngIf=\"createEmpForm != null && !createEmpForm.get('email').valid && createEmpForm.get('email').touched && createEmpForm.get('email').value\"\n          lines=\"none\" class=\"warningMsg\">\n          <ion-label color=\"danger\" style=\"font-size: 3vw;\" >\n            Please enter a valid email\n          </ion-label>\n        </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item lines=\"none\" class=\"empinputs\">\n            <ion-label position=\"floating\" class=\"floatinglbl\">Phone</ion-label>\n            <ion-input id=\"phone\" class=\"login-inputs\" type=\"text\" formControlName=\"phone\"\n            [(ngModel)]=\"phone\" name=\"phone\" required></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item\n          *ngIf=\"createEmpForm != null && !createEmpForm.get('phone').valid && createEmpForm.get('phone').touched && createEmpForm.get('phone').value\"\n          lines=\"none\" class=\"warningMsg\">\n          <ion-label color=\"danger\" style=\"font-size: 3vw;\" >\n            Please enter a valid phone number\n          </ion-label>\n        </ion-item>\n        </ion-col>\n      </ion-row>\n\n\n      <ion-row>\n        <ion-col>\n          <ion-item lines=\"none\" class=\"empinputs\">\n            <ion-label position=\"floating\" class=\"floatinglbl\">Address</ion-label>\n            <ion-input id=\"address\" class=\"login-inputs\" type=\"text\" formControlName=\"address\"\n            [(ngModel)]=\"address\" name=\"address\" required></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item lines=\"none\" class=\"empinputs\">\n            <ion-label position=\"floating\" class=\"floatinglbl\">Status</ion-label>\n            <!-- <ion-input ></ion-input> -->\n            <ion-select id=\"status\" class=\"login-inputs\" type=\"text\" formControlName=\"status\"\n            [(ngModel)]=\"status\" name=\"status\" required>\n              <ion-select-option value=\"Single\">Single</ion-select-option>\n              <ion-select-option value=\"Married\">Married</ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row style=\"margin-left: 4vw;margin-top: 3vw;margin-bottom: 6vw;\">\n        <ion-col>\n          <ion-button class=\"login-btn\" type=\"submit\"\n            [disabled]=\"empID == null || fname== null || lname == null || gender==null || dob == null || email == null || phone == null || address == null || status == null || !createEmpForm.get('email').valid || !createEmpForm.get('phone').valid || empID == '' || fname== '' || lname == '' || gender=='' || dob == '' || email == '' || phone == '' || address == '' || status == '' \">\n            Create\n          </ion-button>\n        </ion-col>\n      </ion-row>\n\n\n\n    </form>\n\n    \n\n  </ion-grid>\n\n \n\n</ion-content>\n\n<ion-content *ngIf=\"!homedata\">\n  <ion-grid style=\"margin-top: 6vh\">\n    <ion-row style=\"margin-top: 40vh;text-align: center\">\n      <ion-col>\n        <div>\n          <ion-spinner name=\"circles\" style=\"width: 20vw;color: #2B71B6; transform: scale(1.5); flex-direction: column\">\n          </ion-spinner>\n        </div>\n\n      </ion-col>\n    </ion-row>\n\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/create-employee/create-employee-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/create-employee/create-employee-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: CreateEmployeePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateEmployeePageRoutingModule", function() { return CreateEmployeePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _create_employee_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./create-employee.page */ "./src/app/pages/create-employee/create-employee.page.ts");




const routes = [
    {
        path: '',
        component: _create_employee_page__WEBPACK_IMPORTED_MODULE_3__["CreateEmployeePage"]
    }
];
let CreateEmployeePageRoutingModule = class CreateEmployeePageRoutingModule {
};
CreateEmployeePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CreateEmployeePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/create-employee/create-employee.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/create-employee/create-employee.module.ts ***!
  \*****************************************************************/
/*! exports provided: CreateEmployeePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateEmployeePageModule", function() { return CreateEmployeePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _create_employee_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./create-employee-routing.module */ "./src/app/pages/create-employee/create-employee-routing.module.ts");
/* harmony import */ var _create_employee_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./create-employee.page */ "./src/app/pages/create-employee/create-employee.page.ts");








let CreateEmployeePageModule = class CreateEmployeePageModule {
};
CreateEmployeePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _create_employee_routing_module__WEBPACK_IMPORTED_MODULE_5__["CreateEmployeePageRoutingModule"]
        ],
        declarations: [_create_employee_page__WEBPACK_IMPORTED_MODULE_6__["CreateEmployeePage"]]
    })
], CreateEmployeePageModule);



/***/ }),

/***/ "./src/app/pages/create-employee/create-employee.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/create-employee/create-employee.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NyZWF0ZS1lbXBsb3llZS9jcmVhdGUtZW1wbG95ZWUucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/create-employee/create-employee.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/create-employee/create-employee.page.ts ***!
  \***************************************************************/
/*! exports provided: CreateEmployeePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateEmployeePage", function() { return CreateEmployeePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_services_employee_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");








let CreateEmployeePage = class CreateEmployeePage {
    constructor(alertController, empService, formBuilder, router, authenticationService, storage) {
        this.alertController = alertController;
        this.empService = empService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.authenticationService = authenticationService;
        this.storage = storage;
        this.createEmpForm = formBuilder.group({
            empID: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            fname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            lname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            gender: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            dob: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')],
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].pattern('[0-9 -()+]+$')],
            address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            status: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]
        });
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        setTimeout(() => {
            this.homedata = {
                'heading': 'Normal text',
            };
        }, 3000);
    }
    goBack() {
        this.router.navigate(['main-ui']);
    }
    createEmployee() {
        let empdata = {
            empID: this.empID,
            fname: this.fname,
            lname: this.lname,
            gender: this.gender,
            dob: this.dob,
            email: this.email,
            phone: this.phone,
            address: this.address,
            status: this.status
        };
        this.empService.createAnEmployee(empdata).then((res) => {
            console.log(res);
            if (res == "Added") {
                this.successAlert();
            }
            else {
                this.existAlert();
            }
        });
        this.createEmpForm.reset();
    }
    successAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message: 'Employee successfully created!',
                buttons: [{
                        text: 'Ok',
                        handler: () => {
                            this.goBack();
                        }
                    }]
            });
            yield alert.present();
        });
    }
    existAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message: 'Employee ID already exist!',
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
};
CreateEmployeePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: src_app_services_employee_service__WEBPACK_IMPORTED_MODULE_6__["EmployeeService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] }
];
CreateEmployeePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-create-employee',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./create-employee.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/create-employee/create-employee.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./create-employee.page.scss */ "./src/app/pages/create-employee/create-employee.page.scss")).default]
    })
], CreateEmployeePage);



/***/ })

}]);
//# sourceMappingURL=pages-create-employee-create-employee-module-es2015.js.map